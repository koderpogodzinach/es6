function add(a, b) {
  return a + b;
}

function multiply(a, b) {
  return a * b;
}

function call(a, b, callback) {
  return callback(a, b);
}

//console.log(call(10, 3, add));

let nazwa = () => {
  return new Promise((resolve, reject) => resolve(add(2, 3)));
};

let nazwa2 = () => {
  return new Promise((resolve, reject) => resolve(multiply(2, 5)));
};

// nazwa()
//   .then(response => {
//     console.log(response);
//     return nazwa2(); // zwracamy tu funkcje, która zostanie wykonana w kolejnym kroku
//   })
//   .then(response => {
//     console.log(response);
//   });

// callback hell polega na

function getAsync(url) {
  return new Promise((resolve, reject) => {
    var httpReq = new XMLHttpRequest();
    httpReq.onreadystatechange = () => {
      if (httpReq.readyState === 4) {
        if (httpReq.status === 200) {
          resolve(JSON.parse(httpReq.responseText));
        } else {
          reject(new Error(httpReq.statusText));
        }
      }
    };
    httpReq.open("GET", url, true);
    httpReq.send();
  });
}

// w tej funkcji serwer wysle zapytanie pod wskazany adres, dopierow w momecie otrzymania informacji dane zostana zwrocone do funkcji callback, wczesniej trzeba stworzyc ta metode
getAsync("https://jsonplaceholder.typicode.com/posts/1", function callback(
  data
) {
  console.log(data);
});

var randomNumber = Math.floor(Math.random() * 10) + 1;

// oczekiwanie na klika promis

// getAsync("https://jsonplaceholder.typicode.com/posts/1")
//   .then(data => {
//     console.log(data);
//   })
//   .catch(err => {
//     alert(err);
//   });

const getPosts = new Promise((resolve, reject) => {
  if (resolve) {
    fetch("https://jsonplaceholder.typicode.com/posts/1")
      .then(response => response.json())
      .then(json => console.log(json));
  } else {
  }
});

const getTodos = new Promise((resolve, reject) => {
  fetch("https://jsonplaceholder.typicode.com/todos/1")
    .then(response => response.json())
    .then(json => console.log(json));
});

getPosts
  .then(data => {
    console.log(data);
    return getTodos();
  })
  .then(data => {
    console.log(data);
  })
  .catch(err => {
    console.log(err);
  });
