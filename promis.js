//promise to asynchromiczny obiekt zwraca on dwie wartosci
// pierwsza kiedy zostanie on poprawnie obsluzony i druga kiedy bedzie blad mozemy go obsluzyc w indtrukcji catch

const getPosts = new Promise((resolve, reject) => {
  fetch("https://jsonplaceholder.typicode.com/todos/1")
    .then(response => response.json())
    .then(json => {
      console.log(json);
      resolve(json);
    })
    .catch(err => reject(err));
});

getPosts.then(data => {
  console.log(data);
});

//https://www.nafrontendzie.pl/jak-pozbyc-sie-callback-hell
//https://devenv.pl/obietnice-promises-podstawy-jezyka-javascript/

//moj przyklad porownania callback do promise

const add = function(a, b) {
  return a + b;
};
const error = function() {
  console.log("error");
};

//w parametrze funkcji przekazuje dwie funkcje callback
function firstCb(add, error) {
  if (true) {
    add();
  } else {
    error();
  }
}

function secondCb(add, error) {
  if (true) {
    add();
  } else {
    error();
  }
}

function thirdCb(add, error) {
  if (true) {
    add();
  } else {
    error();
  }
}

// a teraz chce je wywolac jako funkcje callback

firstCb(
  () => {
    secondCb(
      () => {
        thirdCb(
          () => {
            // when all async actions are done...
            console.log("all done!");
          },
          function() {
            // handle errors here
          }
        );
      },
      function() {
        // handle errors here
      }
    );
  },
  function() {
    // handle errors here
  }
);
// proble m pojawia się kiedy te funkcje sa w roznych plikach i jest ich duzo

// w promisach mozna to rozwiazac nastepujaco :

function asyncFirst() {
  return new Promise((resolve, reject) => {
    if (true) {
      resolve();
    } else {
      reject();
    }
  });
}

function asyncSecond() {
  return new Promise((resolve, reject) => {
    if (true) {
      resolve();
    } else {
      reject();
    }
  });
}

function asyncThird() {
  return new Promise((resolve, reject) => {
    if (true) {
      resolve();
    } else {
      reject();
    }
  });
}

asyncFirst()
  .then(asyncSecond) // ta funkcja wykona się dopiero wtedy kiedy nastąpi wywolanie metody resolve w asyncFirst
  .then(asyncThird)
  .then(() => {
    // when all async actions are done...
    console.log("all done!");
  })
  .catch(() => {
    // error handling...
  });

// plusem tego rozwiazania jst to że tylko raz musimy zlapac blad

//Najbardziej praktyczy przyklad kiedy musimy polaczyc się z 2 zewnetrznymi api i z jednego
// pobrac nazwe miasta a potem dla tej nazwy miasta, pobrac z innego api temperature
// i nie chcemy czekać aż zrobi się to jedno po drugim, tylko robi się to "równolegle"

// Przyklad w takim pseudo kodzie:
let doShopping = () => {
  return new Promise((resolve, reject) => resolve("Got ingredients"));
};
let mixIngredients = () => {
  return new Promise((resolve, reject) => resolve("Made dough"));
};
let bakeCake = () => {
  return new Promise((resolve, reject) => resolve("Baked cake"));
};

doShopping()
  .then(response => {
    alert(response);
    return mixIngredients();
  })
  .then(response => {
    alert(response);
    return bakeCake();
  })
  .then(response => {
    alert(response);
    alert("Bon appetit");
  });
