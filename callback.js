//Przkazujac jakikolwiek argument do funkcji tym argumentem moze byc tez funkcja
// z tego powodu wynika że funkcja jest first-class object(penoprawnym obiektem)
function f1(a, b, function2) {
  consoloe.log(a + b);
  const c = a + b;
  return function2(c);
}

//callback - funkcja wywolania zwrotnego
// callback - to funkcja, która przekazujemy jako argument do funkcji która wywolujemy

function showPopUp(name, alert) {
  //alert to callback w tym przypadku \
  alert(name);
}

//callback - funkcja która przekazana jest jako argument innej funkcji w celu wywolania jej wewnatrz tej funkcji

//Funkcja wyższego rzedu higher0order function, Higher-level function  - to funkcja do której zostala przkazana inna funkcja w formie callback

// funkcja showPopUp jest w tym przypadku funkcja wyzszego rzedu

function add(a, b) {
  return a + b;
}

function substract(a, b) {
  return a - b;
}

console.log(add(3, 4)); // wyw

//callback
function call(a, b, callback) {
  console.log(`Wprowadzone zostaly wartosci ${a} oraz ${b}`);
  return callback(a, b);
}

console.log(call(10, 3, add)); // w tym miejscu nie wywoluje funkcji add za pomoca add() po prostu przekazuje ja do srodka

// kolejny przypadek callback
function alert1(message) {
  alert(message);
}

function call2(a, callback) {
  console.log(a);
  callback(a); // powinien pokazac wartosc a wprowadzona w parametrze
}

//console.log(call2('bbb',alert1));

// moj autorki przyklad
function myFunction(a, b) {
  return  a + b;
}

function call3(a, b, cb) {
  console.log(a);
  console.log(b);
  return cb(a, b);  // trzba uzywac return
}

//console.log(call3(1, 2, myFunction));

// moj przyklad alert jest wywolywany jako callback 

function alert4(message){
    alert(message);
}

function call4(a,b,cb){  // ostatnim parmaeterm bedzie funkcja callback
    console.log(a+b);
    return cb(a);
}

//console.log(call4(1,2,alert4));


//istnieje mawa funkcji wbudowanych,które mozna podac jako callback 
//np. set interval

function message(){
    console.log("jakas wiadomosc");
}
//w pierwszym parametrze tej funkcji podajemy callback 
//setInterval(message,1000);

//console.log(setInterval); // wywolanie tej funkcji w konsoli

//funkcja anonimowa czyli taka przypisana do zmiennej 
const setMessage = function(){
    console.log("przykladowa wiadomosc");
}

//console.log(setInterval(setMessage,1000));

//metoda addEventListener tez jest funkcja wywolania zwrotnego poniewaz pierwszym elementem jest np click a drugi element to jest klasyczny callback 

//callback - ich wada jest to ze moga się zagniezdzac nie wtedy mowi sie o callback hell 
//  https://www.nafrontendzie.pl/jak-pozbyc-sie-callback-hell