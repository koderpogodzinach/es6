var button = document.getElementById("#button");
var div = document.getElementById("#div");

// pobieranie postow z bazy
function getCompaniesBy() {
  return axios
    .get("https://jsonplaceholder.typicode.com/posts/1")
    .then(response => response.data)
    .then(data => {
      var html = "";

      data.forEach(function(element) {
        html = "<h2>" + element.title + "</h2><p>" + element.body + "</p>";
        postContentEl.insertAdjacentHTML("afterbegin", html);
      });
    })
    .catch(err => console.log(err));
}

//Pobieranie losowego numeru i przekazanie tego za pomoca paramertu

const randomNumber = Math.floor(Math.random() * 10 + 1);

console.log(randomNumber);

// A teraz za pomoca promosow wstawie ta wartosc do pobierania z Api konkretnego posta

// rejest odrzucona
// resolved rozwiazana

//Tworzymy funkcje get post w formie Promise
//const getPosts = new Promise(() => {});

//Trzeba tam obsuzyc 2 wartosci resovled i reject
// const getPosts = new Promise((resolve, reject) => {
//   fetch("https://jsonplaceholder.typicode.com/todos/1")
//     .then(response => response.json()) // to co dostaniemy czyli response parsujemy do formatu json
//     .then(json => console.log(json)); // potem wyswietlamy to na konsole
// });

const getPosts = new Promise((resolve, reject) => {
  fetch("https://jsonplaceholder.typicode.com/posts/1") // polaczenie za pomoca fetch, mozna takze skorzystac z axiosa
    .then(response => response.json()) // to co dostaniemy czyli response parsujemy do formatu json
    .then(json => resolve(json)) // do resolvet czyli wtedy kiedy promis zrealizuje sie poprawnie przekazujemu jsona
    .catch(err => reject(err)); // lapiemy ewentualny blad
});

const getTodos = new Promise((resolve, reject) => {
  fetch("https://jsonplaceholder.typicode.com/todos/1")
    .then(response => response.json()) // to co dostaniemy czyli response parsujemy do formatu json
    .then(json => resolve(json)) // do resolvet czyli wtedy kiedy promis zrealizuje sie poprawnie przekazujemu jsona
    .catch(err => reject(err)); // lapiemy ewentualny blad
});

// to jest wywolanie tej metody
getPosts
  .then(posts => {
    console.log(posts); // wyswietlamy posty w konsoli
    return posts; // zwracamy je aby kolejne bloki mogly je obsluzyc
  })
  .then(posts => {
    // mozemy cos zrobic na tych postach

    //chcemy przejsc dalej tylko w wypadku kiedy nasz promise getToDos zostanie obsluzony
    return getTodos; // teraz je zwracamy dlatego mozemy cos z nimi robic
  })
  .then(todos => {
    console.log(todos);
  })
  .catch(err => {
    console.log(err); // globalne lapanie wyjatkow
  });

// za pomoca callback wygladalo by to chyba tak: 