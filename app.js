console.log("es6");

// Destrukturyzacja  - wyciaganie obiekty i tablice z roznych zmiennych

const myObject = {
  name: "Roman",
  surname: "Dziedzic",
  age: "27"
};

//stara wersja przed es6
console.log(myObject.name);

const { name } = myObject; // po prawej stronie podajemy obiekt z którego wyciagamy klucz
//stworzylismy nowego consta kóra zawiera wartosc name

console.log(name);

//Wyciąganie w funkcji:
//Stara szkoa
function myJob(personalData) {
  console.log(`My name is ${personalData.name} and age is ${personalData.age}`);
}
//aby to wywolac trzeba przkazac obiekt myObject jako paramert funkcji
myJob(myObject);

//Nowe podejscie
function myJob2(personalData) {
  const { name, age } = myObject; // w tym miejscu destrukturyzujemy dlatego nie musimy robić personalData.name
  console.log(`My name is ${name} and age is ${age}`);
}

//najlepsze rozwiazanie
//Jezeli jednak mamy wiele danych do destrukturyzacji najlepiej zrobic to tak jak w powyzszym przykladzie
function myJob3({ name, age }) {
  // destrukturyzacji dokonujemy od razu w tym miejscu
  console.log(`My name is ${name} and age is ${age}`);
}

//wywolujemy ja klasycznie
myJob3(myObject);

//Destrukturyzacja z
const myJobs = {
  name: "Rafal",
  jobs: "Programer",
  formerJobs: {
    first: "farmer",
    second: "waiter"
  },
  age: "23"
};

function myJob4({ formerJobs: { first } }) {
  console.log(`My name is ${name} and my first job is  ${first}`);
}

//Destrukturyzacja na tablicach
const name1 = ["Rafal", "Michal", "kuba"];
const [first, second] = name1; // nazwy nie maja znaczenia elementy są wyciagane po koleji

console.log(first);

//Funkcje strzalkowe
function returnNme(name) {
  console.log("My name is" + name);
}

// jezeli funkcja nie zwracabloku kodu a jedynie proste retrun to nie trzeba go zapisywać, wystarczy jedna linia
const returnName2 = name => `My name is ${name}`;

console.log(returnName2("rafalllll")); // w '' poniewaz przkazujemy Stringa a nie obiekt lub stala
//Arrow funkcjon zmieniy tez podejscie do this poniewaz pobieraja this z najblizszego kontekstu w jakim zostalo ono wywolane, jesli tam go nie znajda to ida lewel wyzej

//ISM  - to specyfikacja,która wyszla nie zaleznie od es6

//przy wykorzystywaniu commonJs robione byo to w ten sposob 
const rafal = require('./rafal');
